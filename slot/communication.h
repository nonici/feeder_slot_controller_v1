/* ************************************************************************** */
/*
  @File Name
    communication.h
 
  @Author
    Flitch

  @Summary
    Declares axis array of structs.

  @Description
    Declares axis array of structs and default values for axes.
 */
/* ************************************************************************** */

#ifndef COMMUNICATION_H    /* Guard against multiple inclusion */
#define COMMUNICATION_H

// *****************************************************************************

#include "slot.h"
#include "../../../tools/can_frame.h"

// *****************************************************************************

//Define #define 

// *****************************************************************************

//Declare

// *****************************************************************************

//Functions
void ComunicationInitialize(void);

void CommunicationHandler(void);

void CommunicationSendFeedMsg(void);


void CommunicationWriteCanReportStatus(uint8_t status);
void CommunicationWriteCanReportKeypad(keypad_key_map_bf_t keymap);
void CommunicationWriteCanHeartbeat(hb_state_t state);

// *****************************************************************************

#endif /* COMMUNICATION_H */

/* *****************************************************************************
 End of File
 */
