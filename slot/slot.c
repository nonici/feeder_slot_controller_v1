// *****************************************************************************
/*
  @File Name
    slot.h
 
  @Author
    Flitch

  @Summary
    Slot core state machine.

  @Description
    Slot core state machine.
 */
// *****************************************************************************

#include "slot.h"

// *****************************************************************************

//Local variables definitions

uint8_t slotAddress = 0xFF;
uint8_t slotNodeId = 0xFF;

uint8_t composedCommand;
uint8_t feederResponse;
bool isCommandActive;
bool isCommandFromKeyboard;
bool isCommandReady;

typedef enum {
    SLOT_STAND_BY,
    SLOT_COMMAND_RECEIVED,
    SLOT_KEY_RECEIVED,
    SLOT_FEEDER_POWERED_ON,
    SLOT_HIGH_POWER_ENABLED,
    SLOT_FEEDER_IS_READY,
    SLOT_SENT_COMMAND_TO_FEEDER,
    SLOT_ACTION_COMPLETED,
    SLOT_ACTION_FAILED,
    SLOT_FEEDER_NOT_DETECTED,
} slot_machine_states_t;

slot_machine_states_t slotPreviousState;
slot_machine_states_t slotCurrentState;

// *****************************************************************************

//Local function definitions 

void ReadSlotAddressAndNodeId(void);

// *****************************************************************************

//Core function

//Global functions

//Local functions

void SlotInitialize(void) {
    //Get Slot address, decide on CAN NODE ID
    ReadSlotAddressAndNodeId();

    //Start CAN (exit standby mode)
    TMR1_Initialize();
    TMR1_SetInterruptHandler(MsTimerTick);
    TMR1_StartTimer();

    //Initialize slot state machine
    slotCurrentState = SLOT_STAND_BY;

    FDR_PWR_SEL_5V_SetHigh();
    FDR_PWR_SEL_36V_SetHigh();

    LedRedOn();
    LedYellowOff();
    LedGreenOff();

}

void SlotHandler(void) {

    /* Reads kb
     * Control leds
     * Reads CAN
     * Reads UART
     * Compose FDR command from kb or CAN
     * Reply to CAN if necessary
     * Generate hearth beat signal
     * Periodic checks for feeder present and keyboard pressent
     * Feeder power control and monitor
     * 
     * 
     */

    /*
     * Feeder Power on state machine
     * Power on reasons: periodic check, feed operation     ///Do we even need periodic check?
     * Power off: check done, action completed, action failed, timeout
     * 
     * State: periodic check can result in feeder answering or timeout
     * State feed operation powers on feeder and awaits for response (H_power, L_power, or no response)
     * If no response proceed with error report
     * If HP response increase power and wait for voltage / timeout
     * IF LP active or HP satisfied proceed with feed operation
     * Wait timeout or feeder response (done / error)
     * 
     * 
     */

    if (isCommandReady) {
        isCommandFromKeyboard = 0;
        isCommandReady = 0;
        feederResponse = 0;
        UART1_Write(composedCommand);
    }

    if (UART1_is_rx_ready()) {
        CommunicationWriteCanReportStatus(UART1_Read());
    }

    return;

    switch (slotCurrentState) {

        case SLOT_STAND_BY:
            if (isCommandReady) {
                slotCurrentState = SLOT_COMMAND_RECEIVED;
                feederResponse = 0;
            }
            break;

        case SLOT_COMMAND_RECEIVED:
            //Power on feeder
            slotCurrentState = SLOT_FEEDER_POWERED_ON;
            break;

        case SLOT_FEEDER_POWERED_ON:
            //Measure voltage here

            //Wait for feeder response
            if (feederResponse == BF_FSR_HELLO_LOW_PWR) {
                //Low power feeder detected
                //Go to next state: SLOT_FEEDER_IS_READY    

            } else if (feederResponse == BF_FSR_HELLO_HIGH_PWR) {

            } //else if { timer elapsed } no feeder detected at all;

            break;

        case SLOT_HIGH_POWER_ENABLED:
            //Timeout: voltage level settle
            //Measure voltage here
            //Go to next state: SLOT_FEEDER_IS_READY
            break;

        case SLOT_FEEDER_IS_READY:
            //Send command
            break;

        case SLOT_SENT_COMMAND_TO_FEEDER:
            //wait response or timeout
            //if timeout action failed
            //if response ok then completed
            break;

        case SLOT_ACTION_COMPLETED:
            //if CAN command send succes, if keyboard do nothing or blink led
            break;

        case SLOT_ACTION_FAILED:
            //if CAN command send succes, if keyboard do nothing or blink led
            break;

    }
}

void ReadSlotAddressAndNodeId(void) {

    slotAddress = !ADR4_GetValue();
    slotAddress |= (!ADR3_GetValue() << 1);
    slotAddress |= (!ADR2_GetValue() << 2);
    slotAddress |= (!ADR1_GetValue() << 3);
    slotAddress |= (!ADR0_GetValue() << 4);

    slotNodeId = CanGetNodeIdFromSlotAddress(slotAddress);
}

uint8_t GetSlotAddress(void) {
    return (slotAddress);
}

void SlotDispatchCommandFromKeyboardToFeeder(feeder_cmd_bf_t command, fdr_lane_id_t lane, fdr_pitch_bf_t pitch) {

    composedCommand = FeederComposeCommand(command, lane, pitch);
    //CommunicationWriteCanReportKeypad(composedCommand);
}

void SlotDispatchCommandFromCanBusToFeeder(uint8_t cmd) {
    composedCommand = cmd;
    isCommandFromKeyboard = 0;
    isCommandReady = 1;
}

bool IsCommandAlreadyActive(void) {
    return (slotCurrentState != SLOT_STAND_BY);
}
/* *****************************************************************************
 End of File
 */
