// *****************************************************************************
/*
  @File Name
    keyboard.h
 
  @Author
    Flitch

  @Summary
    Keyboard interface.

  @Description
    Keyboard interface for slot controller.
 */
// *****************************************************************************

#include "keyboard.h"

#define DEBOUNCE_TIME 500
#define SCAN_TIME 5

// *****************************************************************************

//Local variables definitions

ms_timer_t debounceTimer;
ms_timer_t scanTimer;
keypad_key_map_bf_t keyMap = {0};

// *****************************************************************************

//Local function definitions

// *****************************************************************************

void KeyboardHaldner(void) {

    ScanKeyboard();

    //Abort if keyboard not present
    if (!keyMap.bits.keypadPressent) {
        LedYellowOff();
        return;
    }
    LedYellowOn();

    //Abort if debounce timer is active
    if (MsTimerIsTimerActive(&debounceTimer)) {
        return;
    }

    //compare with default states and executes if change is detected
    if (keyMap.bits.keypadKeyF1 || keyMap.bits.keypadKeyF2 || keyMap.bits.keypadKeyF3 || keyMap.bits.keypadKeyF4) {
        //If any key is pressed, evaluate switches and execute action
        MsTimerDelayMs(&debounceTimer, DEBOUNCE_TIME);
        CommunicationWriteCanReportKeypad(keyMap);
    }
}

void ScanKeyboard(void) {

    //Execute only once every 2 ms
    if (MsTimerIsTimerActive(&scanTimer)) {
        return;
    } else {
        MsTimerDelayMs(&scanTimer, SCAN_TIME);
        //scans key states and writes to debouncing FIFO
        keyMap.bits.keypadKeyF1 = !IOE_L1_GetValue();
        keyMap.bits.keypadKeyF2 = !IOE_L2_GetValue();
        keyMap.bits.keypadKeyF3 = !IOE_L3_GetValue();
        keyMap.bits.keypadKeyF4 = !IOE_L4_GetValue();
        keyMap.bits.keypadSwitchP2 = IOE_L5_GetValue();
        keyMap.bits.keypadSwitchLoop = !IOE_L6_GetValue();
        keyMap.bits.keypadSwitchFw = PORTBbits.RB6; //Maknut negaciju
        keyMap.bits.keypadPressent = !PORTBbits.RB7;
    }
}

//Global functions





//Local functions


/* *****************************************************************************
 End of File
 */
