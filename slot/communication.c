// *****************************************************************************
/*
  @File Name
    communication.h
 
  @Author
    Flitch

  @Summary
    Declares axis array of structs.

  @Description
    Declares axis array of structs.
 */
// *****************************************************************************

#include "communication.h"


// *****************************************************************************

//Local variables definitions


ms_timer_t canHearthbeatTimer;
ms_timer_t ledTimer;

uCAN_MSG hearthBeatMessage;
uCAN_MSG keyboardMessage;

//Message used by user application, multi platform
canbus_msg_t inputMessage = {0};
canbus_msg_t outputMessage = {0};

//Message format used by ECAN driver, this CPU specific
uCAN_MSG receiveDriverMessage = {0};
uCAN_MSG transmitDriverMessage = {0};
uCAN_MSG reportStatusMessage = {0};


// *****************************************************************************

//Local function definitions

void CommunicationParseCan(uCAN_MSG * receiveMessage);

void CommunicationReadCan(void);

// *****************************************************************************

//Core function

void ComunicationInitialize(void) {

    reportStatusMessage.frame.idType = dSTANDARD_CAN_MSG_ID_2_0B;
    reportStatusMessage.frame.id = PDO1TX | CanGetNodeIdFromSlotAddress(GetSlotAddress());
    reportStatusMessage.frame.dlc = 1;
    reportStatusMessage.frame.data0 = 0x0;


    CAN1_SB_SetLow();
    ECAN_Initialize();
}

void CommunicationHandler(void) {

    //Reads incoming messages if any
    CommunicationReadCan();

    if (!MsTimerIsTimerActive(&ledTimer)) {
        LedGreenOff();
        LedRedOff();
        LedYellowOff();
    }

    if (!MsTimerIsTimerActive(&canHearthbeatTimer)) {
        MsTimerDelayMs(&canHearthbeatTimer, (5000 + GetSlotAddress()));
        CommunicationWriteCanHeartbeat(STATE_OPERATIONAL);
    }

}
//Global functions

void CommunicationSendFeedMsg(void) {

    CAN_transmit(&keyboardMessage);
}
//Local functions

void CommunicationReadCan(void) {

    if (CAN_receive(&receiveDriverMessage)) {
        //Translate driver message to app message format
        inputMessage.cobId = receiveDriverMessage.frame.id;
        inputMessage.dlc = receiveDriverMessage.frame.dlc;
        inputMessage.data0 = receiveDriverMessage.frame.data0;
        inputMessage.data1 = receiveDriverMessage.frame.data1;
        inputMessage.data2 = receiveDriverMessage.frame.data2;
        inputMessage.data3 = receiveDriverMessage.frame.data3;
        inputMessage.data4 = receiveDriverMessage.frame.data4;
        inputMessage.data5 = receiveDriverMessage.frame.data5;
        inputMessage.data6 = receiveDriverMessage.frame.data6;
        inputMessage.data7 = receiveDriverMessage.frame.data7;
        CanGetFunctionCodeAndNodeIdFromCobId(&inputMessage);
        MsTimerDelayMs(&ledTimer, 500);
        LedRedOn();

        //Check if message is addressed to this unit
        if (inputMessage.nodeId == 0x00 || inputMessage.nodeId == CanGetNodeIdFromSlotAddress(GetSlotAddress())) {

            if (!inputMessage.nodeId) {
                LedRedOn();
            } else {
                if (inputMessage.functionCode == PDO1RX) {

                    LedGreenOn();
                    SlotDispatchCommandFromCanBusToFeeder(inputMessage.data0);
                }
                //CommunicationParseCan(&receiveMessage);
            }
        }

    }

}

void CommunicationWriteCanReportStatus(uint8_t status) {
    reportStatusMessage.frame.idType = dSTANDARD_CAN_MSG_ID_2_0B;
    reportStatusMessage.frame.id = PDO1TX | CanGetNodeIdFromSlotAddress(GetSlotAddress());
    reportStatusMessage.frame.dlc = 1;
    reportStatusMessage.frame.data0 = status;
    CAN_transmit(&reportStatusMessage);
}

void CommunicationWriteCanReportKeypad(keypad_key_map_bf_t keymap) {
    keyboardMessage.frame.idType = dSTANDARD_CAN_MSG_ID_2_0B;
    keyboardMessage.frame.id = PDO2TX | CanGetNodeIdFromSlotAddress(GetSlotAddress());
    keyboardMessage.frame.dlc = 1;
    keyboardMessage.frame.data0 = keymap.map;
    CAN_transmit(&keyboardMessage);
}

void CommunicationWriteCanHeartbeat(hb_state_t state) {
    hearthBeatMessage.frame.idType = dSTANDARD_CAN_MSG_ID_2_0B;
    hearthBeatMessage.frame.id = NMTHB | CanGetNodeIdFromSlotAddress(GetSlotAddress());
    hearthBeatMessage.frame.dlc = 1;
    hearthBeatMessage.frame.data0 = state;
    CAN_transmit(&hearthBeatMessage);
}

void CommunicationParseCan(uCAN_MSG * receiveMessage) {
    //if (receiveMessage->frame.idType);
    //if (receiveMessage->frame.id);
    //if (receiveMessage->frame.dlc);
    if (!(receiveMessage->frame.data0 == 0)) {
        return;
    } //slot ID
    if (receiveMessage->frame.data1 == 0x2) {
        SlotDispatchCommandFromKeyboardToFeeder(BF_FDR_CMD_ADVANCE, LANE_1, BF_PITCH_MM_04);
        //Command

    }
}


/* *****************************************************************************
 End of File
 */
