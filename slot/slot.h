/* ************************************************************************** */
/*
  @File Name
    slot.h
 
  @Author
    Flitch

  @Summary
    Slot core state machine.

  @Description
    Slot core state machine.
 */
/* ************************************************************************** */

#ifndef SLOT_H      /* Guard against multiple inclusion */
#define SLOT_H

// *****************************************************************************

#include "../mcc_generated_files/mcc.h"
#include "keyboard.h"
#include "../../../tools/feeder_dictionary.h"
#include "../../../tools/ms_timer.h"
#include "communication.h"


// *****************************************************************************

//Define #define 

// *****************************************************************************

//Declare

// *****************************************************************************

//Functions

void SlotInitialize(void);
void SlotHandler(void);
void SlotDispatchCommandFromKeyboardToFeeder(feeder_cmd_bf_t command, fdr_lane_id_t lane, fdr_pitch_bf_t pitch);
void SlotDispatchCommandFromCanBusToFeeder(uint8_t cmd);
bool IsCommandAlreadyActive(void);
uint8_t GetSlotAddress(void);


// *****************************************************************************

#endif /* SLOT_H */

/* *****************************************************************************
 End of File
 */
