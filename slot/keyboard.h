/* ************************************************************************** */
/*
  @File Name
    keyboard.c
 
  @Author
    Flitch

  @Summary
    Keyboard interface.

  @Description
    Keyboard interface for slot controller
 */
/* ************************************************************************** */

#ifndef KEYBOARD_H    /* Guard against multiple inclusion */
#define KEYBOARD_H    

// *****************************************************************************

#include "../mcc_generated_files/mcc.h"
#include "../../../tools/ms_timer.h"
#include "slot.h"


// *****************************************************************************

//Define #define 

// *****************************************************************************

void KeyboardHaldner(void);
void ScanKeyboard(void);

// *****************************************************************************

//Functions

static inline void LedGreenOn(void) {
    LED_G_LAT = 0;
}

static inline void LedYellowOn(void) {
    LED_Y_LAT = 0;
}

static inline void LedRedOn(void) {
    LED_R_LAT = 0;
}

static inline void LedGreenOff(void) {
    LED_G_LAT = 1;
}

static inline void LedYellowOff(void) {
    LED_Y_LAT = 1;
}

static inline void LedRedOff(void) {
    LED_R_LAT = 1;
}
// *****************************************************************************

#endif /* KEYBOARD_H */

/* *****************************************************************************
 End of File
 */
