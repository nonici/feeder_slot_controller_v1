/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.7
        Device            :  PIC18LF25K83
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.31 and above
        MPLAB 	          :  MPLAB X 5.45	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

/**
  Section: Included Files
*/

#include <xc.h>

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set FDR_PWR_READ aliases
#define FDR_PWR_READ_TRIS                 TRISAbits.TRISA0
#define FDR_PWR_READ_LAT                  LATAbits.LATA0
#define FDR_PWR_READ_PORT                 PORTAbits.RA0
#define FDR_PWR_READ_WPU                  WPUAbits.WPUA0
#define FDR_PWR_READ_OD                   ODCONAbits.ODCA0
#define FDR_PWR_READ_ANS                  ANSELAbits.ANSELA0
#define FDR_PWR_READ_SetHigh()            do { LATAbits.LATA0 = 1; } while(0)
#define FDR_PWR_READ_SetLow()             do { LATAbits.LATA0 = 0; } while(0)
#define FDR_PWR_READ_Toggle()             do { LATAbits.LATA0 = ~LATAbits.LATA0; } while(0)
#define FDR_PWR_READ_GetValue()           PORTAbits.RA0
#define FDR_PWR_READ_SetDigitalInput()    do { TRISAbits.TRISA0 = 1; } while(0)
#define FDR_PWR_READ_SetDigitalOutput()   do { TRISAbits.TRISA0 = 0; } while(0)
#define FDR_PWR_READ_SetPullup()          do { WPUAbits.WPUA0 = 1; } while(0)
#define FDR_PWR_READ_ResetPullup()        do { WPUAbits.WPUA0 = 0; } while(0)
#define FDR_PWR_READ_SetPushPull()        do { ODCONAbits.ODCA0 = 0; } while(0)
#define FDR_PWR_READ_SetOpenDrain()       do { ODCONAbits.ODCA0 = 1; } while(0)
#define FDR_PWR_READ_SetAnalogMode()      do { ANSELAbits.ANSELA0 = 1; } while(0)
#define FDR_PWR_READ_SetDigitalMode()     do { ANSELAbits.ANSELA0 = 0; } while(0)

// get/set FDR_PWR_SEL_5V aliases
#define FDR_PWR_SEL_5V_TRIS                 TRISAbits.TRISA1
#define FDR_PWR_SEL_5V_LAT                  LATAbits.LATA1
#define FDR_PWR_SEL_5V_PORT                 PORTAbits.RA1
#define FDR_PWR_SEL_5V_WPU                  WPUAbits.WPUA1
#define FDR_PWR_SEL_5V_OD                   ODCONAbits.ODCA1
#define FDR_PWR_SEL_5V_ANS                  ANSELAbits.ANSELA1
#define FDR_PWR_SEL_5V_SetHigh()            do { LATAbits.LATA1 = 1; } while(0)
#define FDR_PWR_SEL_5V_SetLow()             do { LATAbits.LATA1 = 0; } while(0)
#define FDR_PWR_SEL_5V_Toggle()             do { LATAbits.LATA1 = ~LATAbits.LATA1; } while(0)
#define FDR_PWR_SEL_5V_GetValue()           PORTAbits.RA1
#define FDR_PWR_SEL_5V_SetDigitalInput()    do { TRISAbits.TRISA1 = 1; } while(0)
#define FDR_PWR_SEL_5V_SetDigitalOutput()   do { TRISAbits.TRISA1 = 0; } while(0)
#define FDR_PWR_SEL_5V_SetPullup()          do { WPUAbits.WPUA1 = 1; } while(0)
#define FDR_PWR_SEL_5V_ResetPullup()        do { WPUAbits.WPUA1 = 0; } while(0)
#define FDR_PWR_SEL_5V_SetPushPull()        do { ODCONAbits.ODCA1 = 0; } while(0)
#define FDR_PWR_SEL_5V_SetOpenDrain()       do { ODCONAbits.ODCA1 = 1; } while(0)
#define FDR_PWR_SEL_5V_SetAnalogMode()      do { ANSELAbits.ANSELA1 = 1; } while(0)
#define FDR_PWR_SEL_5V_SetDigitalMode()     do { ANSELAbits.ANSELA1 = 0; } while(0)

// get/set FDR_PWR_SEL_36V aliases
#define FDR_PWR_SEL_36V_TRIS                 TRISAbits.TRISA2
#define FDR_PWR_SEL_36V_LAT                  LATAbits.LATA2
#define FDR_PWR_SEL_36V_PORT                 PORTAbits.RA2
#define FDR_PWR_SEL_36V_WPU                  WPUAbits.WPUA2
#define FDR_PWR_SEL_36V_OD                   ODCONAbits.ODCA2
#define FDR_PWR_SEL_36V_ANS                  ANSELAbits.ANSELA2
#define FDR_PWR_SEL_36V_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define FDR_PWR_SEL_36V_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define FDR_PWR_SEL_36V_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define FDR_PWR_SEL_36V_GetValue()           PORTAbits.RA2
#define FDR_PWR_SEL_36V_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define FDR_PWR_SEL_36V_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define FDR_PWR_SEL_36V_SetPullup()          do { WPUAbits.WPUA2 = 1; } while(0)
#define FDR_PWR_SEL_36V_ResetPullup()        do { WPUAbits.WPUA2 = 0; } while(0)
#define FDR_PWR_SEL_36V_SetPushPull()        do { ODCONAbits.ODCA2 = 0; } while(0)
#define FDR_PWR_SEL_36V_SetOpenDrain()       do { ODCONAbits.ODCA2 = 1; } while(0)
#define FDR_PWR_SEL_36V_SetAnalogMode()      do { ANSELAbits.ANSELA2 = 1; } while(0)
#define FDR_PWR_SEL_36V_SetDigitalMode()     do { ANSELAbits.ANSELA2 = 0; } while(0)

// get/set LED_Y aliases
#define LED_Y_TRIS                 TRISAbits.TRISA3
#define LED_Y_LAT                  LATAbits.LATA3
#define LED_Y_PORT                 PORTAbits.RA3
#define LED_Y_WPU                  WPUAbits.WPUA3
#define LED_Y_OD                   ODCONAbits.ODCA3
#define LED_Y_ANS                  ANSELAbits.ANSELA3
#define LED_Y_SetHigh()            do { LATAbits.LATA3 = 1; } while(0)
#define LED_Y_SetLow()             do { LATAbits.LATA3 = 0; } while(0)
#define LED_Y_Toggle()             do { LATAbits.LATA3 = ~LATAbits.LATA3; } while(0)
#define LED_Y_GetValue()           PORTAbits.RA3
#define LED_Y_SetDigitalInput()    do { TRISAbits.TRISA3 = 1; } while(0)
#define LED_Y_SetDigitalOutput()   do { TRISAbits.TRISA3 = 0; } while(0)
#define LED_Y_SetPullup()          do { WPUAbits.WPUA3 = 1; } while(0)
#define LED_Y_ResetPullup()        do { WPUAbits.WPUA3 = 0; } while(0)
#define LED_Y_SetPushPull()        do { ODCONAbits.ODCA3 = 0; } while(0)
#define LED_Y_SetOpenDrain()       do { ODCONAbits.ODCA3 = 1; } while(0)
#define LED_Y_SetAnalogMode()      do { ANSELAbits.ANSELA3 = 1; } while(0)
#define LED_Y_SetDigitalMode()     do { ANSELAbits.ANSELA3 = 0; } while(0)

// get/set LED_G aliases
#define LED_G_TRIS                 TRISAbits.TRISA4
#define LED_G_LAT                  LATAbits.LATA4
#define LED_G_PORT                 PORTAbits.RA4
#define LED_G_WPU                  WPUAbits.WPUA4
#define LED_G_OD                   ODCONAbits.ODCA4
#define LED_G_ANS                  ANSELAbits.ANSELA4
#define LED_G_SetHigh()            do { LATAbits.LATA4 = 1; } while(0)
#define LED_G_SetLow()             do { LATAbits.LATA4 = 0; } while(0)
#define LED_G_Toggle()             do { LATAbits.LATA4 = ~LATAbits.LATA4; } while(0)
#define LED_G_GetValue()           PORTAbits.RA4
#define LED_G_SetDigitalInput()    do { TRISAbits.TRISA4 = 1; } while(0)
#define LED_G_SetDigitalOutput()   do { TRISAbits.TRISA4 = 0; } while(0)
#define LED_G_SetPullup()          do { WPUAbits.WPUA4 = 1; } while(0)
#define LED_G_ResetPullup()        do { WPUAbits.WPUA4 = 0; } while(0)
#define LED_G_SetPushPull()        do { ODCONAbits.ODCA4 = 0; } while(0)
#define LED_G_SetOpenDrain()       do { ODCONAbits.ODCA4 = 1; } while(0)
#define LED_G_SetAnalogMode()      do { ANSELAbits.ANSELA4 = 1; } while(0)
#define LED_G_SetDigitalMode()     do { ANSELAbits.ANSELA4 = 0; } while(0)

// get/set LED_R aliases
#define LED_R_TRIS                 TRISAbits.TRISA5
#define LED_R_LAT                  LATAbits.LATA5
#define LED_R_PORT                 PORTAbits.RA5
#define LED_R_WPU                  WPUAbits.WPUA5
#define LED_R_OD                   ODCONAbits.ODCA5
#define LED_R_ANS                  ANSELAbits.ANSELA5
#define LED_R_SetHigh()            do { LATAbits.LATA5 = 1; } while(0)
#define LED_R_SetLow()             do { LATAbits.LATA5 = 0; } while(0)
#define LED_R_Toggle()             do { LATAbits.LATA5 = ~LATAbits.LATA5; } while(0)
#define LED_R_GetValue()           PORTAbits.RA5
#define LED_R_SetDigitalInput()    do { TRISAbits.TRISA5 = 1; } while(0)
#define LED_R_SetDigitalOutput()   do { TRISAbits.TRISA5 = 0; } while(0)
#define LED_R_SetPullup()          do { WPUAbits.WPUA5 = 1; } while(0)
#define LED_R_ResetPullup()        do { WPUAbits.WPUA5 = 0; } while(0)
#define LED_R_SetPushPull()        do { ODCONAbits.ODCA5 = 0; } while(0)
#define LED_R_SetOpenDrain()       do { ODCONAbits.ODCA5 = 1; } while(0)
#define LED_R_SetAnalogMode()      do { ANSELAbits.ANSELA5 = 1; } while(0)
#define LED_R_SetDigitalMode()     do { ANSELAbits.ANSELA5 = 0; } while(0)

// get/set ADR1 aliases
#define ADR1_TRIS                 TRISAbits.TRISA6
#define ADR1_LAT                  LATAbits.LATA6
#define ADR1_PORT                 PORTAbits.RA6
#define ADR1_WPU                  WPUAbits.WPUA6
#define ADR1_OD                   ODCONAbits.ODCA6
#define ADR1_ANS                  ANSELAbits.ANSELA6
#define ADR1_SetHigh()            do { LATAbits.LATA6 = 1; } while(0)
#define ADR1_SetLow()             do { LATAbits.LATA6 = 0; } while(0)
#define ADR1_Toggle()             do { LATAbits.LATA6 = ~LATAbits.LATA6; } while(0)
#define ADR1_GetValue()           PORTAbits.RA6
#define ADR1_SetDigitalInput()    do { TRISAbits.TRISA6 = 1; } while(0)
#define ADR1_SetDigitalOutput()   do { TRISAbits.TRISA6 = 0; } while(0)
#define ADR1_SetPullup()          do { WPUAbits.WPUA6 = 1; } while(0)
#define ADR1_ResetPullup()        do { WPUAbits.WPUA6 = 0; } while(0)
#define ADR1_SetPushPull()        do { ODCONAbits.ODCA6 = 0; } while(0)
#define ADR1_SetOpenDrain()       do { ODCONAbits.ODCA6 = 1; } while(0)
#define ADR1_SetAnalogMode()      do { ANSELAbits.ANSELA6 = 1; } while(0)
#define ADR1_SetDigitalMode()     do { ANSELAbits.ANSELA6 = 0; } while(0)

// get/set ADR0 aliases
#define ADR0_TRIS                 TRISAbits.TRISA7
#define ADR0_LAT                  LATAbits.LATA7
#define ADR0_PORT                 PORTAbits.RA7
#define ADR0_WPU                  WPUAbits.WPUA7
#define ADR0_OD                   ODCONAbits.ODCA7
#define ADR0_ANS                  ANSELAbits.ANSELA7
#define ADR0_SetHigh()            do { LATAbits.LATA7 = 1; } while(0)
#define ADR0_SetLow()             do { LATAbits.LATA7 = 0; } while(0)
#define ADR0_Toggle()             do { LATAbits.LATA7 = ~LATAbits.LATA7; } while(0)
#define ADR0_GetValue()           PORTAbits.RA7
#define ADR0_SetDigitalInput()    do { TRISAbits.TRISA7 = 1; } while(0)
#define ADR0_SetDigitalOutput()   do { TRISAbits.TRISA7 = 0; } while(0)
#define ADR0_SetPullup()          do { WPUAbits.WPUA7 = 1; } while(0)
#define ADR0_ResetPullup()        do { WPUAbits.WPUA7 = 0; } while(0)
#define ADR0_SetPushPull()        do { ODCONAbits.ODCA7 = 0; } while(0)
#define ADR0_SetOpenDrain()       do { ODCONAbits.ODCA7 = 1; } while(0)
#define ADR0_SetAnalogMode()      do { ANSELAbits.ANSELA7 = 1; } while(0)
#define ADR0_SetDigitalMode()     do { ANSELAbits.ANSELA7 = 0; } while(0)

// get/set IOE_L3 aliases
#define IOE_L3_TRIS                 TRISBbits.TRISB0
#define IOE_L3_LAT                  LATBbits.LATB0
#define IOE_L3_PORT                 PORTBbits.RB0
#define IOE_L3_WPU                  WPUBbits.WPUB0
#define IOE_L3_OD                   ODCONBbits.ODCB0
#define IOE_L3_ANS                  ANSELBbits.ANSELB0
#define IOE_L3_SetHigh()            do { LATBbits.LATB0 = 1; } while(0)
#define IOE_L3_SetLow()             do { LATBbits.LATB0 = 0; } while(0)
#define IOE_L3_Toggle()             do { LATBbits.LATB0 = ~LATBbits.LATB0; } while(0)
#define IOE_L3_GetValue()           PORTBbits.RB0
#define IOE_L3_SetDigitalInput()    do { TRISBbits.TRISB0 = 1; } while(0)
#define IOE_L3_SetDigitalOutput()   do { TRISBbits.TRISB0 = 0; } while(0)
#define IOE_L3_SetPullup()          do { WPUBbits.WPUB0 = 1; } while(0)
#define IOE_L3_ResetPullup()        do { WPUBbits.WPUB0 = 0; } while(0)
#define IOE_L3_SetPushPull()        do { ODCONBbits.ODCB0 = 0; } while(0)
#define IOE_L3_SetOpenDrain()       do { ODCONBbits.ODCB0 = 1; } while(0)
#define IOE_L3_SetAnalogMode()      do { ANSELBbits.ANSELB0 = 1; } while(0)
#define IOE_L3_SetDigitalMode()     do { ANSELBbits.ANSELB0 = 0; } while(0)

// get/set IOE_L4 aliases
#define IOE_L4_TRIS                 TRISBbits.TRISB1
#define IOE_L4_LAT                  LATBbits.LATB1
#define IOE_L4_PORT                 PORTBbits.RB1
#define IOE_L4_WPU                  WPUBbits.WPUB1
#define IOE_L4_OD                   ODCONBbits.ODCB1
#define IOE_L4_ANS                  ANSELBbits.ANSELB1
#define IOE_L4_SetHigh()            do { LATBbits.LATB1 = 1; } while(0)
#define IOE_L4_SetLow()             do { LATBbits.LATB1 = 0; } while(0)
#define IOE_L4_Toggle()             do { LATBbits.LATB1 = ~LATBbits.LATB1; } while(0)
#define IOE_L4_GetValue()           PORTBbits.RB1
#define IOE_L4_SetDigitalInput()    do { TRISBbits.TRISB1 = 1; } while(0)
#define IOE_L4_SetDigitalOutput()   do { TRISBbits.TRISB1 = 0; } while(0)
#define IOE_L4_SetPullup()          do { WPUBbits.WPUB1 = 1; } while(0)
#define IOE_L4_ResetPullup()        do { WPUBbits.WPUB1 = 0; } while(0)
#define IOE_L4_SetPushPull()        do { ODCONBbits.ODCB1 = 0; } while(0)
#define IOE_L4_SetOpenDrain()       do { ODCONBbits.ODCB1 = 1; } while(0)
#define IOE_L4_SetAnalogMode()      do { ANSELBbits.ANSELB1 = 1; } while(0)
#define IOE_L4_SetDigitalMode()     do { ANSELBbits.ANSELB1 = 0; } while(0)

// get/set IOE_L5 aliases
#define IOE_L5_TRIS                 TRISBbits.TRISB2
#define IOE_L5_LAT                  LATBbits.LATB2
#define IOE_L5_PORT                 PORTBbits.RB2
#define IOE_L5_WPU                  WPUBbits.WPUB2
#define IOE_L5_OD                   ODCONBbits.ODCB2
#define IOE_L5_ANS                  ANSELBbits.ANSELB2
#define IOE_L5_SetHigh()            do { LATBbits.LATB2 = 1; } while(0)
#define IOE_L5_SetLow()             do { LATBbits.LATB2 = 0; } while(0)
#define IOE_L5_Toggle()             do { LATBbits.LATB2 = ~LATBbits.LATB2; } while(0)
#define IOE_L5_GetValue()           PORTBbits.RB2
#define IOE_L5_SetDigitalInput()    do { TRISBbits.TRISB2 = 1; } while(0)
#define IOE_L5_SetDigitalOutput()   do { TRISBbits.TRISB2 = 0; } while(0)
#define IOE_L5_SetPullup()          do { WPUBbits.WPUB2 = 1; } while(0)
#define IOE_L5_ResetPullup()        do { WPUBbits.WPUB2 = 0; } while(0)
#define IOE_L5_SetPushPull()        do { ODCONBbits.ODCB2 = 0; } while(0)
#define IOE_L5_SetOpenDrain()       do { ODCONBbits.ODCB2 = 1; } while(0)
#define IOE_L5_SetAnalogMode()      do { ANSELBbits.ANSELB2 = 1; } while(0)
#define IOE_L5_SetDigitalMode()     do { ANSELBbits.ANSELB2 = 0; } while(0)

// get/set IOE_L6 aliases
#define IOE_L6_TRIS                 TRISBbits.TRISB3
#define IOE_L6_LAT                  LATBbits.LATB3
#define IOE_L6_PORT                 PORTBbits.RB3
#define IOE_L6_WPU                  WPUBbits.WPUB3
#define IOE_L6_OD                   ODCONBbits.ODCB3
#define IOE_L6_ANS                  ANSELBbits.ANSELB3
#define IOE_L6_SetHigh()            do { LATBbits.LATB3 = 1; } while(0)
#define IOE_L6_SetLow()             do { LATBbits.LATB3 = 0; } while(0)
#define IOE_L6_Toggle()             do { LATBbits.LATB3 = ~LATBbits.LATB3; } while(0)
#define IOE_L6_GetValue()           PORTBbits.RB3
#define IOE_L6_SetDigitalInput()    do { TRISBbits.TRISB3 = 1; } while(0)
#define IOE_L6_SetDigitalOutput()   do { TRISBbits.TRISB3 = 0; } while(0)
#define IOE_L6_SetPullup()          do { WPUBbits.WPUB3 = 1; } while(0)
#define IOE_L6_ResetPullup()        do { WPUBbits.WPUB3 = 0; } while(0)
#define IOE_L6_SetPushPull()        do { ODCONBbits.ODCB3 = 0; } while(0)
#define IOE_L6_SetOpenDrain()       do { ODCONBbits.ODCB3 = 1; } while(0)
#define IOE_L6_SetAnalogMode()      do { ANSELBbits.ANSELB3 = 1; } while(0)
#define IOE_L6_SetDigitalMode()     do { ANSELBbits.ANSELB3 = 0; } while(0)

// get/set RB4 procedures
#define RB4_SetHigh()            do { LATBbits.LATB4 = 1; } while(0)
#define RB4_SetLow()             do { LATBbits.LATB4 = 0; } while(0)
#define RB4_Toggle()             do { LATBbits.LATB4 = ~LATBbits.LATB4; } while(0)
#define RB4_GetValue()              PORTBbits.RB4
#define RB4_SetDigitalInput()    do { TRISBbits.TRISB4 = 1; } while(0)
#define RB4_SetDigitalOutput()   do { TRISBbits.TRISB4 = 0; } while(0)
#define RB4_SetPullup()             do { WPUBbits.WPUB4 = 1; } while(0)
#define RB4_ResetPullup()           do { WPUBbits.WPUB4 = 0; } while(0)
#define RB4_SetAnalogMode()         do { ANSELBbits.ANSELB4 = 1; } while(0)
#define RB4_SetDigitalMode()        do { ANSELBbits.ANSELB4 = 0; } while(0)

// get/set RB5 procedures
#define RB5_SetHigh()            do { LATBbits.LATB5 = 1; } while(0)
#define RB5_SetLow()             do { LATBbits.LATB5 = 0; } while(0)
#define RB5_Toggle()             do { LATBbits.LATB5 = ~LATBbits.LATB5; } while(0)
#define RB5_GetValue()              PORTBbits.RB5
#define RB5_SetDigitalInput()    do { TRISBbits.TRISB5 = 1; } while(0)
#define RB5_SetDigitalOutput()   do { TRISBbits.TRISB5 = 0; } while(0)
#define RB5_SetPullup()             do { WPUBbits.WPUB5 = 1; } while(0)
#define RB5_ResetPullup()           do { WPUBbits.WPUB5 = 0; } while(0)
#define RB5_SetAnalogMode()         do { ANSELBbits.ANSELB5 = 1; } while(0)
#define RB5_SetDigitalMode()        do { ANSELBbits.ANSELB5 = 0; } while(0)

// get/set IOE_L7 aliases
#define IOE_L7_TRIS                 TRISBbits.TRISB6
#define IOE_L7_LAT                  LATBbits.LATB6
#define IOE_L7_PORT                 PORTBbits.RB6
#define IOE_L7_WPU                  WPUBbits.WPUB6
#define IOE_L7_OD                   ODCONBbits.ODCB6
#define IOE_L7_ANS                  ANSELBbits.ANSELB6
#define IOE_L7_SetHigh()            do { LATBbits.LATB6 = 1; } while(0)
#define IOE_L7_SetLow()             do { LATBbits.LATB6 = 0; } while(0)
#define IOE_L7_Toggle()             do { LATBbits.LATB6 = ~LATBbits.LATB6; } while(0)
#define IOE_L7_GetValue()           PORTBbits.RB6
#define IOE_L7_SetDigitalInput()    do { TRISBbits.TRISB6 = 1; } while(0)
#define IOE_L7_SetDigitalOutput()   do { TRISBbits.TRISB6 = 0; } while(0)
#define IOE_L7_SetPullup()          do { WPUBbits.WPUB6 = 1; } while(0)
#define IOE_L7_ResetPullup()        do { WPUBbits.WPUB6 = 0; } while(0)
#define IOE_L7_SetPushPull()        do { ODCONBbits.ODCB6 = 0; } while(0)
#define IOE_L7_SetOpenDrain()       do { ODCONBbits.ODCB6 = 1; } while(0)
#define IOE_L7_SetAnalogMode()      do { ANSELBbits.ANSELB6 = 1; } while(0)
#define IOE_L7_SetDigitalMode()     do { ANSELBbits.ANSELB6 = 0; } while(0)

// get/set IOE_L8 aliases
#define IOE_L8_TRIS                 TRISBbits.TRISB7
#define IOE_L8_LAT                  LATBbits.LATB7
#define IOE_L8_PORT                 PORTBbits.RB7
#define IOE_L8_WPU                  WPUBbits.WPUB7
#define IOE_L8_OD                   ODCONBbits.ODCB7
#define IOE_L8_ANS                  ANSELBbits.ANSELB7
#define IOE_L8_SetHigh()            do { LATBbits.LATB7 = 1; } while(0)
#define IOE_L8_SetLow()             do { LATBbits.LATB7 = 0; } while(0)
#define IOE_L8_Toggle()             do { LATBbits.LATB7 = ~LATBbits.LATB7; } while(0)
#define IOE_L8_GetValue()           PORTBbits.RB7
#define IOE_L8_SetDigitalInput()    do { TRISBbits.TRISB7 = 1; } while(0)
#define IOE_L8_SetDigitalOutput()   do { TRISBbits.TRISB7 = 0; } while(0)
#define IOE_L8_SetPullup()          do { WPUBbits.WPUB7 = 1; } while(0)
#define IOE_L8_ResetPullup()        do { WPUBbits.WPUB7 = 0; } while(0)
#define IOE_L8_SetPushPull()        do { ODCONBbits.ODCB7 = 0; } while(0)
#define IOE_L8_SetOpenDrain()       do { ODCONBbits.ODCB7 = 1; } while(0)
#define IOE_L8_SetAnalogMode()      do { ANSELBbits.ANSELB7 = 1; } while(0)
#define IOE_L8_SetDigitalMode()     do { ANSELBbits.ANSELB7 = 0; } while(0)

// get/set ADR2 aliases
#define ADR2_TRIS                 TRISCbits.TRISC0
#define ADR2_LAT                  LATCbits.LATC0
#define ADR2_PORT                 PORTCbits.RC0
#define ADR2_WPU                  WPUCbits.WPUC0
#define ADR2_OD                   ODCONCbits.ODCC0
#define ADR2_ANS                  ANSELCbits.ANSELC0
#define ADR2_SetHigh()            do { LATCbits.LATC0 = 1; } while(0)
#define ADR2_SetLow()             do { LATCbits.LATC0 = 0; } while(0)
#define ADR2_Toggle()             do { LATCbits.LATC0 = ~LATCbits.LATC0; } while(0)
#define ADR2_GetValue()           PORTCbits.RC0
#define ADR2_SetDigitalInput()    do { TRISCbits.TRISC0 = 1; } while(0)
#define ADR2_SetDigitalOutput()   do { TRISCbits.TRISC0 = 0; } while(0)
#define ADR2_SetPullup()          do { WPUCbits.WPUC0 = 1; } while(0)
#define ADR2_ResetPullup()        do { WPUCbits.WPUC0 = 0; } while(0)
#define ADR2_SetPushPull()        do { ODCONCbits.ODCC0 = 0; } while(0)
#define ADR2_SetOpenDrain()       do { ODCONCbits.ODCC0 = 1; } while(0)
#define ADR2_SetAnalogMode()      do { ANSELCbits.ANSELC0 = 1; } while(0)
#define ADR2_SetDigitalMode()     do { ANSELCbits.ANSELC0 = 0; } while(0)

// get/set ADR3 aliases
#define ADR3_TRIS                 TRISCbits.TRISC1
#define ADR3_LAT                  LATCbits.LATC1
#define ADR3_PORT                 PORTCbits.RC1
#define ADR3_WPU                  WPUCbits.WPUC1
#define ADR3_OD                   ODCONCbits.ODCC1
#define ADR3_ANS                  ANSELCbits.ANSELC1
#define ADR3_SetHigh()            do { LATCbits.LATC1 = 1; } while(0)
#define ADR3_SetLow()             do { LATCbits.LATC1 = 0; } while(0)
#define ADR3_Toggle()             do { LATCbits.LATC1 = ~LATCbits.LATC1; } while(0)
#define ADR3_GetValue()           PORTCbits.RC1
#define ADR3_SetDigitalInput()    do { TRISCbits.TRISC1 = 1; } while(0)
#define ADR3_SetDigitalOutput()   do { TRISCbits.TRISC1 = 0; } while(0)
#define ADR3_SetPullup()          do { WPUCbits.WPUC1 = 1; } while(0)
#define ADR3_ResetPullup()        do { WPUCbits.WPUC1 = 0; } while(0)
#define ADR3_SetPushPull()        do { ODCONCbits.ODCC1 = 0; } while(0)
#define ADR3_SetOpenDrain()       do { ODCONCbits.ODCC1 = 1; } while(0)
#define ADR3_SetAnalogMode()      do { ANSELCbits.ANSELC1 = 1; } while(0)
#define ADR3_SetDigitalMode()     do { ANSELCbits.ANSELC1 = 0; } while(0)

// get/set ADR4 aliases
#define ADR4_TRIS                 TRISCbits.TRISC2
#define ADR4_LAT                  LATCbits.LATC2
#define ADR4_PORT                 PORTCbits.RC2
#define ADR4_WPU                  WPUCbits.WPUC2
#define ADR4_OD                   ODCONCbits.ODCC2
#define ADR4_ANS                  ANSELCbits.ANSELC2
#define ADR4_SetHigh()            do { LATCbits.LATC2 = 1; } while(0)
#define ADR4_SetLow()             do { LATCbits.LATC2 = 0; } while(0)
#define ADR4_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define ADR4_GetValue()           PORTCbits.RC2
#define ADR4_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define ADR4_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)
#define ADR4_SetPullup()          do { WPUCbits.WPUC2 = 1; } while(0)
#define ADR4_ResetPullup()        do { WPUCbits.WPUC2 = 0; } while(0)
#define ADR4_SetPushPull()        do { ODCONCbits.ODCC2 = 0; } while(0)
#define ADR4_SetOpenDrain()       do { ODCONCbits.ODCC2 = 1; } while(0)
#define ADR4_SetAnalogMode()      do { ANSELCbits.ANSELC2 = 1; } while(0)
#define ADR4_SetDigitalMode()     do { ANSELCbits.ANSELC2 = 0; } while(0)

// get/set RC3 procedures
#define RC3_SetHigh()            do { LATCbits.LATC3 = 1; } while(0)
#define RC3_SetLow()             do { LATCbits.LATC3 = 0; } while(0)
#define RC3_Toggle()             do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define RC3_GetValue()              PORTCbits.RC3
#define RC3_SetDigitalInput()    do { TRISCbits.TRISC3 = 1; } while(0)
#define RC3_SetDigitalOutput()   do { TRISCbits.TRISC3 = 0; } while(0)
#define RC3_SetPullup()             do { WPUCbits.WPUC3 = 1; } while(0)
#define RC3_ResetPullup()           do { WPUCbits.WPUC3 = 0; } while(0)
#define RC3_SetAnalogMode()         do { ANSELCbits.ANSELC3 = 1; } while(0)
#define RC3_SetDigitalMode()        do { ANSELCbits.ANSELC3 = 0; } while(0)

// get/set RC4 procedures
#define RC4_SetHigh()            do { LATCbits.LATC4 = 1; } while(0)
#define RC4_SetLow()             do { LATCbits.LATC4 = 0; } while(0)
#define RC4_Toggle()             do { LATCbits.LATC4 = ~LATCbits.LATC4; } while(0)
#define RC4_GetValue()              PORTCbits.RC4
#define RC4_SetDigitalInput()    do { TRISCbits.TRISC4 = 1; } while(0)
#define RC4_SetDigitalOutput()   do { TRISCbits.TRISC4 = 0; } while(0)
#define RC4_SetPullup()             do { WPUCbits.WPUC4 = 1; } while(0)
#define RC4_ResetPullup()           do { WPUCbits.WPUC4 = 0; } while(0)
#define RC4_SetAnalogMode()         do { ANSELCbits.ANSELC4 = 1; } while(0)
#define RC4_SetDigitalMode()        do { ANSELCbits.ANSELC4 = 0; } while(0)

// get/set CAN1_SB aliases
#define CAN1_SB_TRIS                 TRISCbits.TRISC5
#define CAN1_SB_LAT                  LATCbits.LATC5
#define CAN1_SB_PORT                 PORTCbits.RC5
#define CAN1_SB_WPU                  WPUCbits.WPUC5
#define CAN1_SB_OD                   ODCONCbits.ODCC5
#define CAN1_SB_ANS                  ANSELCbits.ANSELC5
#define CAN1_SB_SetHigh()            do { LATCbits.LATC5 = 1; } while(0)
#define CAN1_SB_SetLow()             do { LATCbits.LATC5 = 0; } while(0)
#define CAN1_SB_Toggle()             do { LATCbits.LATC5 = ~LATCbits.LATC5; } while(0)
#define CAN1_SB_GetValue()           PORTCbits.RC5
#define CAN1_SB_SetDigitalInput()    do { TRISCbits.TRISC5 = 1; } while(0)
#define CAN1_SB_SetDigitalOutput()   do { TRISCbits.TRISC5 = 0; } while(0)
#define CAN1_SB_SetPullup()          do { WPUCbits.WPUC5 = 1; } while(0)
#define CAN1_SB_ResetPullup()        do { WPUCbits.WPUC5 = 0; } while(0)
#define CAN1_SB_SetPushPull()        do { ODCONCbits.ODCC5 = 0; } while(0)
#define CAN1_SB_SetOpenDrain()       do { ODCONCbits.ODCC5 = 1; } while(0)
#define CAN1_SB_SetAnalogMode()      do { ANSELCbits.ANSELC5 = 1; } while(0)
#define CAN1_SB_SetDigitalMode()     do { ANSELCbits.ANSELC5 = 0; } while(0)

// get/set IOE_L1 aliases
#define IOE_L1_TRIS                 TRISCbits.TRISC6
#define IOE_L1_LAT                  LATCbits.LATC6
#define IOE_L1_PORT                 PORTCbits.RC6
#define IOE_L1_WPU                  WPUCbits.WPUC6
#define IOE_L1_OD                   ODCONCbits.ODCC6
#define IOE_L1_ANS                  ANSELCbits.ANSELC6
#define IOE_L1_SetHigh()            do { LATCbits.LATC6 = 1; } while(0)
#define IOE_L1_SetLow()             do { LATCbits.LATC6 = 0; } while(0)
#define IOE_L1_Toggle()             do { LATCbits.LATC6 = ~LATCbits.LATC6; } while(0)
#define IOE_L1_GetValue()           PORTCbits.RC6
#define IOE_L1_SetDigitalInput()    do { TRISCbits.TRISC6 = 1; } while(0)
#define IOE_L1_SetDigitalOutput()   do { TRISCbits.TRISC6 = 0; } while(0)
#define IOE_L1_SetPullup()          do { WPUCbits.WPUC6 = 1; } while(0)
#define IOE_L1_ResetPullup()        do { WPUCbits.WPUC6 = 0; } while(0)
#define IOE_L1_SetPushPull()        do { ODCONCbits.ODCC6 = 0; } while(0)
#define IOE_L1_SetOpenDrain()       do { ODCONCbits.ODCC6 = 1; } while(0)
#define IOE_L1_SetAnalogMode()      do { ANSELCbits.ANSELC6 = 1; } while(0)
#define IOE_L1_SetDigitalMode()     do { ANSELCbits.ANSELC6 = 0; } while(0)

// get/set IOE_L2 aliases
#define IOE_L2_TRIS                 TRISCbits.TRISC7
#define IOE_L2_LAT                  LATCbits.LATC7
#define IOE_L2_PORT                 PORTCbits.RC7
#define IOE_L2_WPU                  WPUCbits.WPUC7
#define IOE_L2_OD                   ODCONCbits.ODCC7
#define IOE_L2_ANS                  ANSELCbits.ANSELC7
#define IOE_L2_SetHigh()            do { LATCbits.LATC7 = 1; } while(0)
#define IOE_L2_SetLow()             do { LATCbits.LATC7 = 0; } while(0)
#define IOE_L2_Toggle()             do { LATCbits.LATC7 = ~LATCbits.LATC7; } while(0)
#define IOE_L2_GetValue()           PORTCbits.RC7
#define IOE_L2_SetDigitalInput()    do { TRISCbits.TRISC7 = 1; } while(0)
#define IOE_L2_SetDigitalOutput()   do { TRISCbits.TRISC7 = 0; } while(0)
#define IOE_L2_SetPullup()          do { WPUCbits.WPUC7 = 1; } while(0)
#define IOE_L2_ResetPullup()        do { WPUCbits.WPUC7 = 0; } while(0)
#define IOE_L2_SetPushPull()        do { ODCONCbits.ODCC7 = 0; } while(0)
#define IOE_L2_SetOpenDrain()       do { ODCONCbits.ODCC7 = 1; } while(0)
#define IOE_L2_SetAnalogMode()      do { ANSELCbits.ANSELC7 = 1; } while(0)
#define IOE_L2_SetDigitalMode()     do { ANSELCbits.ANSELC7 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/